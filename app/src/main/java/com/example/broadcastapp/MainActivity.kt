package com.example.broadcastapp

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.BatteryManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.example.broadcastapp.ui.theme.BroadcastAppTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment

class MainActivity : ComponentActivity() {
    private val viewModel: CounterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val batteryIntent: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            applicationContext.registerReceiver(null, ifilter)
        }

        val br = TimeBroadcastReceiver(viewModel)

        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION).apply {
            addAction(Intent.ACTION_TIME_TICK)
        }
        registerReceiver(br, filter)

        setContent {
            BroadcastAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    color = MaterialTheme.colorScheme.background
                ) {
                    var activeCounting by rememberSaveable { mutableStateOf(true) }
                    var count by rememberSaveable { mutableStateOf(0) }

                    if (batteryIntent?.getBooleanExtra(
                            BatteryManager.EXTRA_BATTERY_LOW, false)!!
                    ) {
                        Text(
                            text = "Charge me, please! I'm about to die",
                            textAlign = TextAlign.Center,
                            modifier = Modifier.fillMaxSize(),
                        )
                    } else {
                        Column(
                            modifier = Modifier.fillMaxSize(),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            if (activeCounting) {
                                val state = br!!.viewModel.state
                                count = state.counter

                                SimpleScreen(
                                    text = "Waiting for $count min",
                                    btnText = "The final countdown!"
                                ) {
                                    unregisterReceiver(br)
                                    activeCounting = false
                                }
                            } else {
                                SimpleScreen(
                                    text = "Last time waiting is $count min",
                                    btnText = "Start counting!",
                                ) {
                                    activeCounting = true
                                    registerReceiver(br, filter)
                                    br!!.viewModel.reset()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun SimpleScreen(
    text: String,
    btnText: String,
    btnOnClick: () -> Unit
) {
    Text(
        text = text,
        textAlign = TextAlign.Center,
    )
    Button(onClick = btnOnClick) {
        Text(text = btnText)
    }
}
