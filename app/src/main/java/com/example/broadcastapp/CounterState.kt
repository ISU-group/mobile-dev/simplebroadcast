package com.example.broadcastapp

data class CounterState(
    var counter: Int = -1
)
