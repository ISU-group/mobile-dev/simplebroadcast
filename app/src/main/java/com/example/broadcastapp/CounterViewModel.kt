package com.example.broadcastapp

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue


class CounterViewModel : ViewModel() {
    var state by mutableStateOf(CounterState())
        private set

    fun increment() {
        state = CounterState(state.counter + 1)
    }

    fun reset() {
        state = CounterState()
    }
}
