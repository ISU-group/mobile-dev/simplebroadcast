package com.example.broadcastapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class TimeBroadcastReceiver(
    val viewModel: CounterViewModel
) : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.let {
            StringBuilder().apply {
                append("Action: ${it.action}\n")
                append("URI: ${it.toUri(Intent.URI_INTENT_SCHEME)}\n")
                toString().also {
                    viewModel.increment()
                }
            }
        }
    }

    companion object {
        const val TAG: String = "TimeBroadcastReceiver"
    }
}